<?php

namespace Marketplace\App\Cloud\Config;

use Marketplace\App\Kernel;

/**
 * Class Upload
 * @package Marketplace\App\Cloud\Config
 */
class Upload {

	/**
	 * @return mixed
	 */
	public static function getConfig() {
		$out = Kernel\Config::getFile( 'service.file.upload' );

		return $out['service']['upload'];
	}

	/**
	 * @param $status
	 *
	 * @return array
	 */
	public static function getMime( $status ) {
		$get = self::getConfig();

		$out = $get['mime'][ $status ] ?? '' ?: [];

		return $out;
	}
}